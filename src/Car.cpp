#include "Car.h"
#include "Tool.h"
#include <math.h>
#include "cinder\gl\gl.h"

const float Car::length = 36.0f;
const float Car::width = 12.0f;
const float Car::mass = 5.0f;
const ci::Color Car::col = ci::Color(1.0f, 1.0f, 1.0f);

const float Car::cDrag = 0.4257f;
const float Car::cRollingResistance = 12.8f;

const float Car::cTransmissionEfficiency = 0.7f;

const float Car::cWheelRadius = 0.5f;

float Car::separationWeight;
float Car::separationRadius;

Car::Car(ci::Vec2f carStartingLocation, float carStartingAngle, ci::Path2d *path)
{
	frontAxle = Axle(length / 3.0f, width / 2.0f, cWheelRadius);
	rearAxle = Axle(-length / 3.0f, width / 2.0f, cWheelRadius);
	breakLights = false;

	position = carStartingLocation;
	velocity = ci::Vec2f(0.0f, 0.0f);
	acceleration = ci::Vec2f(0.0f, 0.0f);
	force = ci::Vec2f(0.0f, 0.0f);

	angle = carStartingAngle;
	angularVelocity = 0.0f;
	angularAcceleration = 0.0f;
	// the equation actually states that moment of inertia formula uses car's length and length, fix?
	inertia = (1.0f / 12.0f) * (((width / 2.0f) * (width / 2.0f)) + ((length / 2.0f) * (length / 2.0f))) * mass;
	torque = 0.0f;

	rpm = 0.0f;

	/* Initialising gears lookup table */
	float g1, g2, g3, g4, g5, g6, gR;
	g1 = 2.66f;
	g2 = 1.78f;
	g3 = 1.30f;
	g4 = 1.0f;
	g5 = 0.74f;
	g6 = 0.50f;
	gR = 2.90f;
	xDifferential = 3.42f;
	gearsTable.push_back(gR);
	gearsTable.push_back(g1);
	gearsTable.push_back(g2);
	gearsTable.push_back(g3);
	gearsTable.push_back(g4);
	gearsTable.push_back(g5);
	gearsTable.push_back(g6);

	/* Initialising torque lookup table */
	ci::Vec2f val1, val2, val3, val4, val5, val6;
	val1 = ci::Vec2f(1000.0f, 395.0f);
	val2 = ci::Vec2f(2000.0f, 435.0f);
	val3 = ci::Vec2f(3000.0f, 450.0f);
	val4 = ci::Vec2f(4000.0f, 475.0f);
	val5 = ci::Vec2f(5000.0f, 460.0f);
	val6 = ci::Vec2f(6000.0f, 380.0f);
	torqueTable.push_back(val1);
	torqueTable.push_back(val2);
	torqueTable.push_back(val3);
	torqueTable.push_back(val4);
	torqueTable.push_back(val5);
	torqueTable.push_back(val6);

	/*	Path following	*/
	current = 0;
	float distance = position.distance(path->getPoint(0));
	int counter = 0;
	for (unsigned int i = 0; i < path->getPoints().size(); i++) {
		float distanceToIthPoint = position.distance(path->getPoint(i));
		if (distanceToIthPoint < distance)
		{
			distance = distanceToIthPoint;
			counter = i;
		}
	}
	current = counter;
}


Car::~Car()
{
}

void Car::draw(bool drawSeekVector, bool drawSeparationForceVector, bool drawAccelerationVector, bool drawVelocityVector, bool drawSeparationRadius)
{
	angle = tool::normaliseAngle(angle, -M_PI, M_PI);
	ci::gl::color(col);
	ci::gl::pushMatrices();
	ci::gl::translate(position);
	ci::gl::rotate(ci::toDegrees(angle));
	ci::gl::drawStrokedRect(ci::Rectf(ci::Vec2f(-width, -length) / 2.0f, ci::Vec2f(width, length) / 2.0f));
	frontAxle.draw();
	rearAxle.draw();
	ci::gl::drawSolidCircle(ci::Vec2f(0.0f, 0.0f), 1.0f);
	if (breakLights)
	{
		ci::gl::color(ci::Color(1.0f, 0.0f, 0.0f));
		ci::gl::drawLine(ci::Vec2f(-width * 0.9, -length) / 2.0f, ci::Vec2f(-width * 0.3, -length) / 2.0f);
		ci::gl::drawLine(ci::Vec2f(width * 0.9, -length) / 2.0f, ci::Vec2f(width * 0.3, -length) / 2.0f);
	}
	ci::gl::popMatrices();

	/* drawing vectors */
	float arrowLength = length * 0.6f;
	ci::Vec3f loc(position, 0.0f);

	if (drawSeparationRadius)
	{
		ci::gl::color(ci::Color(1.0f, 0.0f, 1.0f));
		ci::gl::drawStrokedCircle(position, separationRadius);
	}

	if (drawSeekVector)
	{
		ci::Vec2f seekNormalised = follow;
		seekNormalised.safeNormalize();
		ci::Vec3f seekNormalisedVector(seekNormalised, 0.0f);
		ci::gl::color(ci::Color(0.0f, 1.0f, 0.0f));
		ci::gl::drawVector(loc, loc + seekNormalisedVector * arrowLength, 5.0f, 5.0f);
	}

	if (drawAccelerationVector)
	{
		ci::Vec2f accelerationNormalised = acceleration;
		accelerationNormalised.safeNormalize();
		ci::Vec3f accelerationVector(accelerationNormalised, 0.0f);
		ci::gl::color(ci::Color(1.0f, 0.0f, 0.0f));
		ci::gl::drawVector(loc, loc + accelerationVector * arrowLength, 5.0f, 5.0f);
	}

	if (drawSeparationForceVector)
	{
		ci::Vec2f separationNormalised = separation;
		separationNormalised.safeNormalize();
		ci::Vec3f separationVector(separationNormalised, 0.0f);
		ci::gl::color(ci::Color(0.0f, 0.0f, 1.0f));
		ci::gl::drawVector(loc, loc + separationVector * arrowLength, 5.0f, 5.0f);
	}

	if (drawVelocityVector)
	{
		ci::Vec3f velocityVector(velocity, 0.0f);
		velocityVector.normalize();
		ci::gl::color(ci::Color(1.0f, 1.0f, 0.0f));
		ci::gl::drawVector(loc, loc + velocityVector * arrowLength, 5.0f, 5.0f);
	}
}

void Car::update(float dTime, ci::Vec2i cursor, float throttle, float steering, float breaks, std::list<Car> *cars, ci::Path2d *path)
{
	breakLights = false;
	follow = pathFollowing(path) - position;
	// break if nearing the next point in path and if approaching too fast
	if (follow.length() < 150.0f && velocity.length() > 100.0f) {
		rearAxle.setBrakes(1.0f);
		frontAxle.setBrakes(1.0f);
		breakLights = true;
	}
	else {
		follow.normalize();

		separation = separate(cars);
		ci::Vec2f separationTranslated = tool::worldToRelative(separation, angle);
		float separationAngle = std::atan2(separationTranslated.y, separationTranslated.x);
		// break if there are cars in front of you that is a danger
		if (separationAngle > M_PI || separationAngle < 0) {
			rearAxle.setBrakes(breaks);
			frontAxle.setBrakes(breaks);
			breakLights = true;
		}

		ci::Vec2f steer = follow + (separation * separationWeight);
		steer.normalize();

		steer = tool::worldToRelative(steer, angle);

		float steerAngle = -M_PI / 2.0f + std::atan2f(steer.y, steer.x);
		steerAngle = tool::normaliseAngle(steerAngle, -M_PI, M_PI);
		if (steerAngle > 1.0f)
			steerAngle = 1.0f;
		else if (steerAngle < -1.0f)
			steerAngle = -1.0f;

		rearAxle.setThrottle(1.0f);
		frontAxle.setSteering(steerAngle);
		rearAxle.setBrakes(breaks);
		frontAxle.setBrakes(breaks);
	}

	rearAxle.update(dTime, angularVelocity, velocity, angle);
	addForce(rearAxle.leftWheel.worldResponseForce, rearAxle.leftWheel.worldWheelOffset);
	addForce(rearAxle.rightWheel.worldResponseForce, rearAxle.rightWheel.worldWheelOffset);
	frontAxle.update(dTime, angularVelocity, velocity, angle);
	addForce(frontAxle.leftWheel.worldResponseForce, frontAxle.leftWheel.worldWheelOffset);
	addForce(frontAxle.rightWheel.worldResponseForce, frontAxle.rightWheel.worldWheelOffset);

	acceleration = force / mass;
	velocity += acceleration * dTime;
	position += velocity * dTime;
	force = ci::Vec2f(0.0f, 0.0f);

	angularAcceleration = torque / inertia;
	angularVelocity += angularAcceleration * dTime;
	angle += angularVelocity * dTime;
	torque = 0.0f;
}

ci::Vec2f Car::separate(std::list<Car> *cars)
{
	ci::Vec2f steer = ci::Vec2f(0.0f, 0.0f);
	int count = 0;
	// For every Car in the simulation, check if it's too close
	for (std::list<Car>::iterator iter = cars->begin(); iter != cars->end(); ++iter)
	{
		Car other = *iter;
		float d = position.distance(other.position);
		// If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
		if ((d > 0) && (d < separationRadius)) {
			// Calculate vector pointing away from neighbor
			ci::Vec2f diff = position - other.position;
			diff.normalize();
			diff /= d;			// Weight by distance
			steer += diff;
			count++;            // Keep track of how many
		}
	}
	// Average -- divide by how many
	if (count > 0) {
		steer /= (float)count;
	}

	// As long as the vector is greater than 0

	return steer.safeNormalized();
}

void Car::addForce(ci::Vec2f linerForce, ci::Vec2f angularForce)
{
	//add linar force
	force += linerForce;
	//and it's associated torque
	torque += angularForce.cross(linerForce);
}

ci::Vec2f Car::pathFollowing(ci::Path2d *path)
{
	ci::Vec2f target = ci::Vec2f();
	target = path->getPoint(current);

	if (position.distance(target) <= 40.0f) {
		current++;
		if (current >= path->getPoints().size()) {
			current = 0;
		}
	}
	return target;
}

float Car::shiftGears()
{
	return gearsTable.at(1);
}

float Car::lookupTorqueCurve(float rpm)
{
	ci::Vec2f upper, lower = ci::Vec2f(0.0f, 0.0f);
	for (unsigned int i = 0; i < torqueTable.size(); i++)
	{
		if (rpm == torqueTable.at(i).x)
		{
			return torqueTable.at(i).y;
		}
		else if (rpm < torqueTable.at(i).x)
		{
			upper = torqueTable.at(i);
			lower = torqueTable.at(i - 1);
			break;
		}
	}
	return lower.y + (upper.y - lower.y) * (rpm - lower.x) / (upper.x - lower.x);
}