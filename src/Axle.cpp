#include "Axle.h"
#include "Tool.h"
#include <math.h>

Axle::Axle()
{
}

Axle::Axle(float height, float width, float wheelSize)
{
	leftWheel = Wheel(ci::Vec2f(-width, height), wheelSize);
	rightWheel = Wheel(ci::Vec2f(width, height), wheelSize);
	leftWheel.setSteeringAngle(0.0f);
	rightWheel.setSteeringAngle(0.0f);
}

void Axle::setSteering(float steering)
{
	const float steeringLock = 0.6981f;

	//apply steering angle to front wheels
	leftWheel.setSteeringAngle(-steering * steeringLock);
	rightWheel.setSteeringAngle(-steering * steeringLock);
}

void Axle::setThrottle(float throttle)
{
	const float torque = 20.0f;

	//apply transmission torque to back wheels
	leftWheel.addTransmissionTorque(throttle * torque);
	rightWheel.addTransmissionTorque(throttle * torque);
}

void Axle::setBrakes(float brakes)
{
	const float brakeTorque = 4.0f;

	//apply brake torque apposing wheel vel
	float wheelVel = leftWheel.m_wheelSpeed;
	leftWheel.addTransmissionTorque(-wheelVel * brakeTorque * brakes);
	wheelVel = rightWheel.m_wheelSpeed;
	rightWheel.addTransmissionTorque(-wheelVel * brakeTorque * brakes);
}

void Axle::draw()
{
	leftWheel.draw();
	rightWheel.draw();
}

void Axle::update(float timeStep, float m_angularVelocity, ci::Vec2f m_velocity, float m_angle)
{
	//wheel.m_wheelSpeed = 30.0f;
	leftWheel.update(timeStep, m_angularVelocity, m_velocity, m_angle);
	rightWheel.update(timeStep, m_angularVelocity, m_velocity, m_angle);
}

