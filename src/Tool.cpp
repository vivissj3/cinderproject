#include "Tool.h"
#include "cinder\Matrix.h"

ci::Vec2f tool::relativeToWorld(ci::Vec2f vector, float angle)
{
	ci::Matrix33<float> mat = ci::Matrix33<float>();
	mat = mat.identity();
	mat.rotate(ci::Vec3f::zAxis(), angle);

	ci::Vec3f v = mat.transformVec(ci::Vec3f(vector, 0.0f));
	return ci::Vec2f(v.x, v.y);
}

ci::Vec2f tool::worldToRelative(ci::Vec2f vector, float angle)
{
	ci::Matrix33<float> mat = ci::Matrix33<float>();
	mat = mat.identity();
	mat.rotate(ci::Vec3f::zAxis(), -angle);

	ci::Vec3f v = mat.transformVec(ci::Vec3f(vector, 0.0f));
	return ci::Vec2f(v.x, v.y);
}

ci::Vec2f tool::project(ci::Vec2f v1, ci::Vec2f v2)
{
	float crossProduct = v1.x * v2.x + v1.y * v2.y;
	return v2 * crossProduct;
}

ci::Vec2f tool::project(ci::Vec2f v1, ci::Vec2f v2, float *mag)
{
	float crossProduct = v1.x * v2.x + v1.y * v2.y;
	*mag = crossProduct;
	return v2 * crossProduct;
}

float tool::normaliseAngle(float angle, float lowerBound, float upperBound)
{
	float val = ci::toDegrees(angle);
	lowerBound = ci::toDegrees(lowerBound);
	upperBound = ci::toDegrees(upperBound);
	if (lowerBound > upperBound){ std::swap(lowerBound, upperBound); }
	val -= lowerBound; //adjust to 0
	double rangeSize = upperBound - lowerBound;
	if (rangeSize == 0){ return upperBound; } //avoid dividing by 0
	return ci::toRadians(val - (rangeSize * std::floor(val / rangeSize)) + lowerBound);
}

ci::Vec2f tool::pointVel(ci::Vec2f worldOffset, float m_angularVelocity, ci::Vec2f m_velocity)
{
	ci::Vec2f tangent = ci::Vec2f(-worldOffset.y, worldOffset.x);
	return tangent * m_angularVelocity + m_velocity;
}