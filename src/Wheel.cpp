#include "Wheel.h"
#include "Axle.h"
#include "Tool.h"
#include <math.h>

const ci::Rectf Wheel::wheelSize = ci::Rectf(-ci::Vec2f(2.0f, 3.0f), ci::Vec2f(2.0f, 3.0f));
const ci::Color Wheel::col = ci::Color(1.0f, 1.0f, 1.0f);

Wheel::Wheel()
{
}

Wheel::Wheel(ci::Vec2f position, float radius)
{
	worldResponseForce = ci::Vec2f(0.0f, 0.0f);
	worldWheelOffset = ci::Vec2f(0.0f, 0.0f);
	m_Position = position;
	m_wheelSpeed = 0.0f;
	m_wheelRadius = radius;
	m_wheelInertia = radius * radius; //fake value
	m_wheelTorque = 0.0f;
}

void Wheel::setSteeringAngle(float newAngle)
{
	ci::Matrix33<float> mat = ci::Matrix33<float>();
	mat = mat.identity();
	ci::Vec3f forwardVector = ci::Vec3f(0.0f, 1.0f, 0.0f);
	ci::Vec3f sideVector = ci::Vec3f(-1.0f, 0.0f, 0.0f);

	mat.rotate(-ci::Vec3f::zAxis(), newAngle);
	forwardVector = mat.transformVec(forwardVector);
	sideVector = mat.transformVec(sideVector);

	m_forwardAxis = ci::Vec2f(forwardVector.x, forwardVector.y);
	m_sideAxis = ci::Vec2f(sideVector.x, sideVector.y);
}

void Wheel::addTransmissionTorque(float newValue)
{
	m_wheelTorque += newValue;
}

ci::Vec2f Wheel::calculateForce(ci::Vec2f relativeGroundSpeed, float timeStep)
{
	//calculate speed of tire patch at ground
	ci::Vec2f patchSpeed = -m_forwardAxis * m_wheelSpeed * m_wheelRadius;

	//get velocity difference between ground and patch
	ci::Vec2f velDifference = relativeGroundSpeed + patchSpeed;

	//project ground speed onto side axis
	float forwardMag = 0.0f;
	ci::Vec2f sideVel = tool::project(velDifference, m_sideAxis);
	ci::Vec2f forwardVel = tool::project(velDifference, m_forwardAxis, &forwardMag);

	//calculate super fake friction forces
	//calculate response force
	ci::Vec2f responseForce = -sideVel * 2.0f;
	responseForce -= forwardVel;

	//calculate torque on wheel
	m_wheelTorque += forwardMag * m_wheelRadius;

	//integrate total torque into wheel
	m_wheelSpeed += m_wheelTorque / m_wheelInertia * timeStep;

	//clear our transmission torque accumulator
	m_wheelTorque = 0.0f;

	//return force acting on body
	return responseForce;
}

void Wheel::update(float timeStep, float m_angularVelocity, ci::Vec2f m_velocity, float m_angle)
{
	worldWheelOffset = tool::relativeToWorld(m_Position, m_angle);
	ci::Vec2f worldGroundVel = tool::pointVel(worldWheelOffset, m_angularVelocity, m_velocity);
	ci::Vec2f relativeGroundSpeed = tool::worldToRelative(worldGroundVel, m_angle);
	ci::Vec2f relativeResponseForce = calculateForce(relativeGroundSpeed, timeStep);
	worldResponseForce = tool::relativeToWorld(relativeResponseForce, m_angle);
}

void Wheel::draw()
{
	ci::gl::color(col);
	ci::gl::pushMatrices();
	ci::gl::translate(m_Position);

	float wheelAngle = -M_PI / 2.0f + std::atan2f(m_forwardAxis.y, m_forwardAxis.x);
	ci::gl::rotate(ci::toDegrees(wheelAngle));
	ci::gl::drawSolidRoundedRect(wheelSize, 1.0f, 0);
	ci::gl::popMatrices();

	//ci::gl::pushMatrices();
	//ci::gl::translate(m_Position);
	//float arrowLength = 45.0f;
	//ci::Vec3f v1(m_forwardAxis, 0.0f);
	//ci::Vec3f v2(m_sideAxis, 0.0f);
	//ci::gl::color(ci::Color(1.0f, 0.0f, 0.0f));
	//ci::gl::drawVector(ci::Vec3f(0.0f, 0.0f, 0.0f), v1 * arrowLength, 5.0f, 5.0f);
	//ci::gl::drawVector(ci::Vec3f(0.0f, 0.0f, 0.0f), v2 * arrowLength, 5.0f, 5.0f);
	//ci::gl::popMatrices();
}