#include "Controller.h"
#include "cinder/Rand.h"
#include "cinder/gl/gl.h"

Controller::Controller() {
	float pX = 80.0f;
	float pY = 640.0f;
	path = ci::Path2d();
	path.moveTo(ci::Vec2f(pX, pY));
	for (int i = 1; i < 8; i++) {
		path.lineTo(ci::Vec2f(pX + i * 150, pY + cos(i) * 50));
	}
	pX = 1200.0f;
	pY = 80.0f;
	path.lineTo(ci::Vec2f(pX, pY));
	for (int i = 1; i < 8; i++) {
		path.lineTo(ci::Vec2f(pX - i * 150, pY + cos(i) * 50));
	}
	path.close();
	
	for (int i = 0; i < 15; i++) {
		cars.push_back(Car(ci::Vec2f(ci::Rand::randFloat(1280), ci::Rand::randFloat(720)), ci::Rand::randFloat(-M_PI, M_PI), &path));
	}
}

void Controller::update(ci::Vec2i mouseLocation, float deltaTime, float dSeparationWeight, float dSeparationRadius, float throttle, float steering, float breaks)
{
	Car::separationWeight = dSeparationWeight;
	Car::separationRadius = dSeparationRadius;
	for (std::list<Car>::iterator iter = cars.begin(); iter != cars.end(); ++iter)
	{
		iter->update(deltaTime, mouseLocation, throttle, steering, breaks, &cars, &path);
	}
}

void Controller::draw(bool dCursorPullVector, bool dSeparationVector, bool dAccelerationVector, bool dVelocityVector, bool drawSeparationRadius)
{
	for (std::list<Car>::iterator iter = cars.begin(); iter != cars.end(); ++iter)
	{
		iter->draw(dCursorPullVector, dSeparationVector, dAccelerationVector, dVelocityVector, drawSeparationRadius);
	}
	ci::gl::color(ci::Color::hex(0xCCFF33));
	ci::gl::draw(path);
	ci::gl::color(ci::Color::hex(0xFFFFFF));
	for (signed int i = 0; i < path.getPoints().size(); i++)
	{
		ci::gl::drawSolidCircle(path.getPoint(i), 3.0f, 5);
	}
}

void Controller::add(ci::Vec2i mouseLocation)
{
	//mBoids.push_back(Boid(ci::Vec2f(mouseLocation.x, mouseLocation.y)));
}