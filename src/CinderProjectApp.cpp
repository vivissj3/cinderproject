#include "cinder/app/AppNative.h"
#include "cinder/params/Params.h"
#include "cinder/Display.h"
#include "cinder/Vector.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include "Controller.h"
#include <math.h>


class CinderProjectApp : public ci::app::AppNative {
  public:
	int screenW;
	int screenH;
	void prepareSettings(Settings *settings);
	void setup();
	void mouseMove(cinder::app::MouseEvent event);
	void mouseDown(cinder::app::MouseEvent event);
	void keyDown(cinder::app::KeyEvent event);
	void keyUp(cinder::app::KeyEvent event);
	void update();
	void draw();

	ci::Vec2i mMouseLoc;
	Controller controller;

	ci::params::InterfaceGlRef params;

	bool dCursorPullVector;
	bool dSeparationVector;
	bool dAccelerationVector;
	bool dVelocityVector;
	bool drawSeparationRadius;

	float dSeparationWeight;
	float dSeparationRadius;

	float lastTime;
	float deltaTime;

	float steering;
	float throttle;
	float breaks;
};

void CinderProjectApp::prepareSettings(Settings *settings)
{
	screenW = 1280;
	screenH = 720;
	std::cout << screenW + "x" + screenH;
	settings->setWindowSize(screenW, screenH);
	settings->setFrameRate(60.0f);
}

void CinderProjectApp::setup()
{
	steering = 0.0f;
	throttle = 0.0f;
	breaks = 0.0f;

	mMouseLoc = ci::Vec2i(0, 0);

	// SETUP PARAMS
	
	dCursorPullVector = false;
	dSeparationVector = false;
	dAccelerationVector = false;
	dVelocityVector = false;

	dSeparationWeight = 1.0f;
	dSeparationRadius = 48.0f;

	drawSeparationRadius = false;

	int debugWidth = 240;
	int debugHeight = 160;
	params = ci::params::InterfaceGl::create("Debug", ci::Vec2i(debugWidth, debugHeight));
	params->setPosition(ci::Vec2i((int)(ci::app::getWindowWidth() - debugWidth) * 0.95f, (int)(ci::app::getWindowHeight() - debugHeight) * 0.95f));
	params->addParam("Seeking vector", &dCursorPullVector, "");
	params->addSeparator();
	params->addParam("Separation vector", &dSeparationVector, "");
	params->addParam("Separation weight", &dSeparationWeight, "");
	params->addParam("Draw separation radius", &drawSeparationRadius, "");
	params->addParam("Separation radius", &dSeparationRadius, "");
	params->addSeparator();
	params->addParam("Acceleration vector", &dAccelerationVector, "");
	params->addSeparator();
	params->addParam("Velocity vector", &dVelocityVector, "");
	
	controller = Controller();
	lastTime = (float) cinder::app::getElapsedSeconds();
}

void CinderProjectApp::mouseMove(cinder::app::MouseEvent event)
{
	mMouseLoc = event.getPos();
}

void CinderProjectApp::mouseDown(cinder::app::MouseEvent event)
{
	mMouseLoc = event.getPos();
	controller.add(mMouseLoc);
}

void CinderProjectApp::keyDown(cinder::app::KeyEvent event)
{
	if (event.getChar() == 'w'){
		throttle = 1.0f;
	}
	else if (event.getChar() == 's'){
		breaks = 1.0f;
	}
	else if (event.getChar() == 'a'){
		steering = -1.0f;
	}
	else if (event.getChar() == 'd'){
		steering = 1.0f;
	}
}

void CinderProjectApp::keyUp(cinder::app::KeyEvent event)
{
	if (event.getChar() == 'w'){
		throttle = 0.0f;
	}
	else if (event.getChar() == 's'){
		breaks = 0.0f;
	}
	else if (event.getChar() == 'a'){
		steering = 0.0f;
	}
	else if (event.getChar() == 'd'){
		steering = 0.0f;
	}
}

void CinderProjectApp::update()
{
	float currentTime = (float) cinder::app::getElapsedSeconds();
	deltaTime = (currentTime - lastTime);
	lastTime = currentTime;
	controller.update(mMouseLoc, deltaTime, dSeparationWeight, dSeparationRadius, throttle, steering, breaks);
	
}

void CinderProjectApp::draw()
{
	// clear out the window with blue
	ci::gl::clear(ci::Color::hex(0x003366));
	controller.draw(dCursorPullVector, dSeparationVector, dAccelerationVector, dVelocityVector, drawSeparationRadius);

	// DRAW PARAMS WINDOW
	params->draw();

	
	std::string time = std::to_string(lastTime);
	time += "s";
	std::string fps = std::to_string(ci::app::App::getAverageFps());
	fps += " FPS";
	ci::app::getWindow()->setTitle(time + " " + fps);
}

CINDER_APP_NATIVE(CinderProjectApp, ci::app::RendererGl)
