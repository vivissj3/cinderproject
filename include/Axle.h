#pragma once
#include "Wheel.h"
#include "cinder\Vector.h"

class Axle
{
public:
	Wheel leftWheel;
	Wheel rightWheel;

	Axle();
	Axle(float height, float width, float wheelSize);
	/**
	*	Updates the axle's status.
	*
	*	@param timeStep The change in time, in seconds
	*	@param m_angularVelocity Car's angular velocity
	*	@param m_velocity
	*	@param m_angle
	*/
	void update(float timeStep, float m_angularVelocity, ci::Vec2f m_velocity, float m_angle);
	/**
	 *	Calls wheels drawing methods. 
	 */
	void draw();

	/**
	 *	Sets throttle of both wheels.
	 *
	 *	@param throttle New throttle
	 */
	void setThrottle(float throttle);

	/**
	 *	Sets steering angle of both wheels.
	 *
	 *	@param steering New steering angle in radians
	 */
	void setSteering(float steering);

	/**
	 *	Sets breaks of both wheels.
	 *
	 *	@param brakes 0.0 meaning the brakes are not applied, 1.0 brakes are fully applied
	 */
	void setBrakes(float brakes);
};

