#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/Path2d.h"
#include "Car.h"
#include <list>

class Controller {
public:
	std::list<Car> cars;
	ci::Path2d path;

	Controller();
	/*
	 *	Updates all objects belonging to the simulation.
	 *
	 *	@param mouseLocation Cursor postion, used for debugging and testing
	 *	@param deltaTime Change in time since the last update, in seconds
	 *	@param dSeparationWeight Weight of the separation force
	 *	@param dSeparationRadius Radius of the separation force
	 *	@param throttle Used for testing with input from keyboard
	 *	@param steering Used for testing with input from keyboard
	 *	@param breaks Used for testing with input from keyboard
	 */
	void update(ci::Vec2i mouseLocation, float deltaTime, float dSeparationWeight, float dSeparationRadius, float throttle, float steering, float breaks);
	/*
	 *	Draws all objects belonging to the simulation.
	 *
	 *	@param dSeekVector Should seek vector be drawn
	 *	@param dSeparationVector Should separation vector be drawn
	 *	@param dAccelerationVector Should acceleration vector be drawn
	 *	@param dVelocityVector Should velocity vector be drawn
	 *	@param drawSeparationRadius Should separation radius be drawn
	 */
	void draw(bool dSeekVector, bool dSeparationVector, bool dAccelerationVector, bool dVelocityVector, bool drawSeparationRadius);
	
	/*
	 *	Add new object to the simulation.
	 *
	 *	@param location Where the object should be placed
	 */
	void add(ci::Vec2i location);
};