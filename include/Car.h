#pragma once
#include <list>
#include <vector>
#include "Axle.h"
#include "cinder\Color.h"
#include "cinder\Path2d.h"
#include "cinder\Vector.h"

class Car
{
public:
	Car(ci::Vec2f carStartingLocation, float carStartingAngle, ci::Path2d *path);
	~Car();

	/**
	 *	Draw method to put the car on the screen.
	 *
	 *	@param drawSeekVector Should seeking vector be drawn
	 *	@param drawSeparationForceVector Should separation vector be drawn
	 *	@param drawAccelerationVector Should acceleration vector be drawn
	 *	@param drawVelocityVector Should velocity vector be drawn
	 *	@param drawSeparationRadius Should should separation radius be drawn
	 */
	void draw(bool drawCursorPullVector, bool drawSeparationForceVector, bool drawAccelerationVector, bool drawVelocityVector, bool drawSeparationRadius);
	/**
	 *	Updates car's postion.
	 *
	 *	Runs update methods for all the Axles/Wheels, then , then calculates the forces on the car's body
	 *	and calculates updated position.
	 *
	 *	@param dTime Change in time since the last update, in seconds
	 *	@param cursor Cursor postion, used for debugging and testing
	 *	@param throttle Used for testing with input from keyboard
	 *	@param steering Used for testing with input from keyboard
	 *	@param breaks Used for testing with input from keyboard
	 *	@param *cars pointer to the list of all the other Cars, for calculating separation, etc.
	 *	@param *path pointer to the path for the car to follow
	 */
	void update(float dTime, ci::Vec2i cursor, float throttle, float steering, float breaks, std::list<Car> *cars, ci::Path2d *path);
	static float separationWeight;
	static float separationRadius;

private:
	/*				Physical elements	*/
	Axle frontAxle;
	Axle rearAxle;
	bool breakLights;

	/*				Motion variables	*/
	ci::Vec2f position;
	ci::Vec2f velocity;
	ci::Vec2f acceleration;
	ci::Vec2f force;

	float angle;
	float angularVelocity;
	float angularAcceleration;
	float inertia;
	float torque;

	/*				Constants			*/
	static const ci::Color col;
	static const float length;
	static const float width;
	static const float mass;
	static const float cDrag;
	static const float cRollingResistance;
	static const float cTransmissionEfficiency;
	static const float cWheelRadius;


	/*				Forces				*/
	ci::Vec2f fDrag;					// Air resistance
	ci::Vec2f fTraction;
	ci::Vec2f fRollingResistance;

	/*				Torque(s)			*/
	std::vector<ci::Vec2f> torqueTable;

	/*				Rotation rate(s)	*/
	float rpm;

	/*				Ratio(s)			*/
	float xGear;
	float xDifferential;
	std::vector<float> gearsTable;

	/*				Methods				*/

	/**
	 *	Controlls shifting the gears.
	 *
	 *	Controlls shifting gears of the car, for the moment unimplemented, returns only the first gear.
	 *
	 *	@return Appropriate gear for the car's current actions
	 */
	float shiftGears();
	/**
	 *	Looks up torque for given rpm.
	 *
	 *	Uses interpolation to look up a torque value from the torqueTable, for a given RPM value.
	 *
	 *	@param rpm RPM value to look up the torque for
	 *	@return Toruqe value for given RPM
	 */
	float lookupTorqueCurve(float rpm);

	/**
	 *	Adds forces to the car's body.
	 *
	 *	Adds linear and angular forces coming from the wheels and acting on the car's body.
	 *
	 *	@param linerForce The linear force acting on the car's body
	 *	@param angularForce The angular forces acting on the car's body
	 */
	void addForce(ci::Vec2f worldForce, ci::Vec2f worldOffset);

	/**
	 *	Calculates separation force.
	 *
	 *	Calculates a vector away from other Car objects which are within the radius of separationRadius
	 *
	 *	@param *cars Pointer to the list of cars to check this car's distance against and calculate the force if necessary.
	 *	@return Vec2f Vector away from cars within the separationRadius
	 */
	ci::Vec2f separate(std::list<Car> *cars);

	/*				Steering vectors	*/
	ci::Vec2f separation;
	ci::Vec2f follow;

	int current;
	/**
	 *	Returns the next target on the path.
	 *
	 *	Takes points of the pathway and returns the next one in order.
	 *
	 *	@param *path Pointer to the pathway
	 */
	ci::Vec2f pathFollowing(ci::Path2d *path);
};

