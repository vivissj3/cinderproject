#pragma once
#include "cinder\Vector.h"

namespace tool {
	/*
		
	*/
	template <typename T> static int sgn(T);
	/**
	 *	Normalise an angle between two values.
	 *
	 *	@param angle Angle to be normalised, in radians
	 *	@param lowerBound of the range , in radians
	 *	@param upperBound of the range , in radians
	 *	@return normalised angle
	 */
	float normaliseAngle(float val, float lowerBound, float upperBound);
	/**
	 *	Transform a vector from simulation's world to the local space.
	 *
	 *	@param vector Vector to be transformed
	 *	@param angle Angle of rotation by which the vector will be transformed, in radians
	 *	@return Vector transformed to local space
	 */
	ci::Vec2f relativeToWorld(ci::Vec2f relative, float m_angle);
	/**
	 *	Transform a vector from simulation's world to the local space.
	 *
	 *	@param vector Vector to be transformed
	 *	@param angle Angle of rotation by which the vector will be transformed, in radians
	 *	@return Vector transformed to local space
	 */
	ci::Vec2f worldToRelative(ci::Vec2f world, float m_angle);
	/**
	 *	Project vector v1 on v2.
	 *
	 *	@param v1 Vector to be projected
	 *	@param v2 Vector to be projected onto
	 *	@return Projection of v1 on v2
	 */
	ci::Vec2f project(ci::Vec2f v1, ci::Vec2f v2);
	/**
	 *	Project vector v1 on v2.
	 *
	 *	@param v1 Vector to be projected
	 *	@param v2 Vector to be projected onto
	 *	@param *mag Magnitude
	 *	@return Projection of v1 on v2
	 */
	ci::Vec2f project(ci::Vec2f v1, ci::Vec2f v2, float *mag);
	/**
	 *	Calculates velocity of a point on a body.
	 *
	 *	@param worldOffset Offset of the body from the world
	 *	@param m_angularVelocity Angular velocity of the body
	 *	@param m_velocity Velocity of the body
	 *	@return Velocity of the point
	 */
	ci::Vec2f pointVel(ci::Vec2f worldOffset, float m_angularVelocity, ci::Vec2f m_velocity);
};

template <typename T> int tool::sgn(T val) {
	return (T(0) < val) - (val < T(0));
}