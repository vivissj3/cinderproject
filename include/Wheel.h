#pragma once
#include "cinder\Vector.h"
#include "cinder\gl\gl.h"

class Wheel
{
public:
	Wheel();
	Wheel(ci::Vec2f, float);

	ci::Vec2f worldWheelOffset;
	ci::Vec2f worldResponseForce;

	ci::Vec2f m_forwardAxis;
	ci::Vec2f m_sideAxis;
	ci::Vec2f m_Position;

	float m_wheelTorque;
	float m_wheelSpeed;
	float m_wheelInertia;
	float m_wheelRadius;

	static const ci::Rectf wheelSize;
	static const ci::Color col;

	/**
	 *	Updates the wheel's status.
	 *
	 *	@param timeStep The change in time, in seconds
	 *	@param m_angularVelocity Car's angular velocity
	 *	@param m_velocity
	 *	@param m_angle
	 */
	void update(float timeStep, float m_angularVelocity, ci::Vec2f m_velocity, float m_angle);

	/**
	 *	Draws the wheel to the screen.
	 */
	void draw();

	/**
	 *	Sets this wheel's steering angle.
	 *
	 *	Performs a transformation of wheel's sideAxis and forwardAxis by the angle of rotation.
	 *
	 *	@param newAngle New angle for the steering
	 */
	void setSteeringAngle(float newAngle);
	/**
	 *	Adds torque.
	 *
	 *	@param Value by which wheelTorque will be incremented
	 */
	void addTransmissionTorque(float newValue);

	/**
	 *	Calculates forces exerted by the wheel.
	 *
	 *	@param relativeGroundSpeed Speed of the ground relative to the wheel
	 *	@param timeStep The change in time, in seconds
	 *	@return Vector of the force exterted from this wheel
	 */
	ci::Vec2f calculateForce(ci::Vec2f relativeGroundSpeed, float timeStep);
};